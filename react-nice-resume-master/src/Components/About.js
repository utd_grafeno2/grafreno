import React, { Component } from "react";
import Fade from "react-reveal";

class About extends Component {
  render() {
    if (!this.props.data) return null;

    const name = this.props.data.name;
    const profilepic = "images/" + this.props.data.image;
    const bio = this.props.data.bio;
    const street = this.props.data.address.street;
    const city = this.props.data.address.city;
    const state = this.props.data.address.state;
    const zip = this.props.data.address.zip;
    const phone = this.props.data.phone;
    const email = this.props.data.email;
    const resumeDownload = this.props.data.resumedownload;
    const bio2 = this.props.data.bio2;

    return (
      <section id="about">
        <Fade duration={1000}>
          <div className="row">
            <div className="three columns">
              <img
                className="profile-pic"
                src={profilepic}
                alt="GRAFENO ENTERPRISE"
              />
            </div>
            <h2>NOSOTROS</h2>
            <div className="nine columns main-col">
              <h2>Misión</h2>

              <p>{bio}</p>

              <h2>Visión</h2>

              <p>{bio2}</p>
            
            </div>
          </div>
        </Fade>
      </section>
    );
  }
}

export default About;
